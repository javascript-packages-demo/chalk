# [chalk](https://www.npmjs.com/package/chalk)

Terminal string styling done right

## See also
* [travis-util/ANSI-escape-code](https://github.com/travis-util/ANSI-escape-code)

### Chalk family